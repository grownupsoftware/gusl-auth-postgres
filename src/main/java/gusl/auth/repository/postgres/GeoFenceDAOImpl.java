package gusl.auth.repository.postgres;

import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.repository.GeoFenceDAO;
import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;
import org.jvnet.hk2.annotations.Service;

@Service
@PostgresTable(table = "geo_fence", type = GeoFenceDO.class, staticContent = "geo_fence.json", version = GeoFenceDO.VERSION)
public class GeoFenceDAOImpl extends AbstractDAOPostgresImpl<GeoFenceDO> implements GeoFenceDAO {

    public GeoFenceDAOImpl() {
        super(GeoFenceDO.class);
    }
}
