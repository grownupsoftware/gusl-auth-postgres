package gusl.auth.repository.postgres;

import gusl.auth.model.roles.RoleDO;
import gusl.auth.repository.RoleDAO;
import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;
import org.jvnet.hk2.annotations.Service;

@Service
@PostgresTable(table = "role", type = RoleDO.class, staticContent = "role.json", version = RoleDO.VERSION)
public class RoleDAOImpl extends AbstractDAOPostgresImpl<RoleDO> implements RoleDAO {

    public RoleDAOImpl() {
        super(RoleDO.class);
    }

}
