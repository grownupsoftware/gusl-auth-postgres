package gusl.auth.repository.postgres;

import gusl.auth.model.session.SessionDO;
import gusl.auth.model.session.SessionStatus;

import gusl.auth.repository.SessionDAO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;
import gusl.query.*;
import org.jvnet.hk2.annotations.Service;

import java.util.List;

@Service
@PostgresTable(table = "session", type = SessionDO.class, version = SessionDO.VERSION)
public class SessionDAOImpl extends AbstractDAOPostgresImpl<SessionDO> implements SessionDAO {

    private final UpdateParams STATUS_UPDATE = UpdateParams.builder().updateField("status").build();

    public SessionDAOImpl() {
        super(SessionDO.class);
    }

    public List<SessionDO> getAllForUser(String betterId) throws GUSLErrorException {
        return get(QueryParams.builder()
                .must(MatchQuery.of("bettorId", betterId))
                .orderBy(OrderBy.of("dateUpdated", QueryOrder.ASC))
                .limit(100)
                .build());
    }

    @Override
    public SessionDO updateStatus(String id, SessionStatus newStatus) throws GUSLErrorException {
        SessionDO delta = new SessionDO();
        delta.setId(id);
        delta.setStatus(newStatus);
        return update(delta, STATUS_UPDATE);
    }
}
