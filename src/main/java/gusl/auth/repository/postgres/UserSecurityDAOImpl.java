package gusl.auth.repository.postgres;

import gusl.auth.model.security.UserSecurityCacheEvent;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.repository.UserSecurityDAO;
import gusl.model.cache.CacheAction;
import gusl.node.service.InternalMessagingService;
import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Service
@PostgresTable(table = "user_security", type = UserSecurityDO.class, staticContent = "user_security.json", version = UserSecurityDO.VERSION)
public class UserSecurityDAOImpl extends AbstractDAOPostgresImpl<UserSecurityDO> implements UserSecurityDAO {

    @Inject
    private InternalMessagingService theMessagingService;

    public UserSecurityDAOImpl() {
        super(UserSecurityDO.class);
        setUpdateConsumer(entity -> theMessagingService.broadcastEventReportException(new UserSecurityCacheEvent(CacheAction.UPDATE, entity)));
        setDeleteConsumer(entity -> theMessagingService.broadcastEventReportException(new UserSecurityCacheEvent(CacheAction.DELETE, entity)));
        setInsertConsumer(entity -> theMessagingService.broadcastEventReportException(new UserSecurityCacheEvent(CacheAction.ADD, entity)));
    }

}
